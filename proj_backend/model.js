module.exports = (sequelize, Sequelize) => {
    const Books = sequelize.define("books", {
        title: {
            type: Sequelize.STRING,
        },
        author: {
            type: Sequelize.STRING,
        },
        phone: {
            type: Sequelize.INTEGER,
            
        },
        email: {
            type: Sequelize.STRING,
            unique: true,
            validate: {
                isEmail: true
            }
        },
        category: {
            type: Sequelize.STRING,
        },
        sdesc: {
            type: Sequelize.STRING,
        },
        published: {
            type: Sequelize.BOOLEAN,
        }
    });
    return Books;
};