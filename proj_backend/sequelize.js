// const dbConfig = require('./dbconfig')
const {HOST,PORT,USER,PASSWORD,DB,dialect} = require('./dbconfig')
const Sequelize = require("sequelize");
const sequelize = new Sequelize(DB, USER,PASSWORD, {
    host: HOST,
    port: PORT,
    dialect: dialect,
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.books = require('./model')(sequelize, Sequelize);
module.exports = db





sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
