const express = require("express")

const bookRoute = require('./routes/book_routes')
const db = require("./sequelize");
const fileUpload = require("express-fileupload");

const app = express()

db.sequelize.sync().then(() => {
    console.log("synced with database");
});

app.use(express.json())

app.use(fileUpload({ createParentPath: true }));

app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin','*')
    res.setHeader('Access-Control-Allow-Headers','Origin,X-Requested-With,Content-Type,Accept')
    res.setHeader('Access-Control-Allow-Methods','GET,POST,DELETE,PUT,PATCH,OPTIONS')

    next()
})

app.use('/book', bookRoute)

app.use(function (err, req, res, next) {
    // console.log('U called?')
    res.status(err.status || 500);
    res.send(err);
    
});

app.listen(3000)